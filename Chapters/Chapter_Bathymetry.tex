% Chapter Template

\chapter{Cost models and bathymetry} % Main chapter title

\label{Chapter_Bathymetry} % Change X to a consecutive number; for referencing this chapter elsewhere, use \ref{ChapterX}

\lhead{Chapter VII. \emph{Cost models and bathymetry}} % Change X to a consecutive number; this is for the header on each page - perhaps a shortened title

%----------------------------------------------------------------------------------------
%	SECTION 1
%----------------------------------------------------------------------------------------

\section{Cost model}
% TODO: foundation costs
When dealing with wind farm development process it is crucial to properly position individual turbines. Previous chapters presented results obtained using two layout models, one where the turbine's location was described only using two cartesian coordinates and another one where they were arranged in a lattice. 

In the first case the turbines could be moved around arbitrarily and in fact their position was constrained by the external boundary polygon as well as changing AEP due to wakes caused by proximity of another turbine. In the latter case position of the lattice with respect to the datum could be largely arbitrary and the most important factors influencing the energy production were those describing shape of the lattice, as well as its orientation towards the predominant wind direction. Adding an information about the ground depth below water level can have an impact on the position of the lattice within the boundary. 

This chapter contains the information about inclusion of bathymetry to the optimization problem, mainly in the form of combining water depth with energy production.

The chosen cost model represents a linear combination of cost and energy production. It can represent the installation costs being offset by profits from electricity sales. Similar cost models can be found in \cite{rethore2014TOPMuloptwinfar_2}, \cite{samorani2013winfarlayoptpro}.

While this cost model is simple, it enables demonstration of coupling between the annual energy production and water depth. It is also possible to add the information about water depth in a form of constraints, for example by requiring that the turbines should be placed at a position where the depth is smaller than a specified value.

\section{Site description}
%
\begin{figure}[htbp!]
  \centering
    \includegraphics[width=\textwidth]{Figures/Bathymetry_depth_field.png}
  \caption{Water depth}
  \label{fig:Bathymetry_depth_field}
\end{figure}
%

The data containing bathymetry is plotted in figure \ref{fig:Bathymetry_depth_field}. The data itself was collected in a grid with 100 m spacing in both directions. In order to obtain its value at an arbitrary position I used spline interpolation.

The derivatives of the underwater terrain depth with respect to position were calculated using first order central difference and forward/backward difference on the boundary. The resulting plots for respective derivatives can be seen in \ref{fig:Bathymetry_gradients}. This was done to ensure no jump discontinuities were present, which could lead to reaching an infeasible position by the optimizer.

%
\begin{figure}[htbp!]
  \centering
    \includegraphics[width=\textwidth]{Figures/Bathymetry_gradients.png}
  \caption{Depth function gradients}
  \label{fig:Bathymetry_gradients}
\end{figure}
%


\section{Simple layout}
The optimization problem presented in chapter \ref{Chapter2} was modified to include a cost model as an objective function. The annual energy production was combined with the water depth using weight factors $a$ and $b$. This is shown in eq.\ref{eq:Bathymetry_formulation_simple}.

\begin{equation}
    \label{eq:Bathymetry_formulation_simple}
    \begin{aligned}
        & \text{maximize} & & \sum_{i = 0}^N \left( a AEP_i(x)  - b f_{depth}(x_i, y_i) \right) \\
        & \text{subject to} & & c_{boundary, lower} \le c_{boundary}(x_i, y_i) \le c_{boundary, upper} \\
        &  & & c_{proximity, lower} \le c_{proximity}(x) \le c_{proximity, upper} \\
        & \text{where:} & & x = \begin{bmatrix}
                         x_1, \ldots, x_n, y_1, \ldots, y_n
                     \end{bmatrix}, i = 1, \ldots, n
    \end{aligned}
\end{equation}

The gradient of the objective function can be calculated in the following way:
\begin{align}
    \nabla f_{obj} = a \nabla AEP - b \nabla f_{depth}
\end{align}

%
\begin{figure}[htbp!]
  \centering
    \includegraphics[width=\textwidth]{Figures/Bathymetry_layout_simple.png}
  \caption{Optimized layout using simple position model}
  \label{fig:Bathymetry_layout_simple}
\end{figure}
%

An optimized wind farm layout for this problem is presented in figure \ref{fig:Bathymetry_layout_simple}. The optimization process was conducted using 16 Vestas V80-2.0MW turbines which were initially placed randomly inside the boundary. The lower bounds of the proximity constraints were set to 560 m. The same wind climate as presented in chapter \ref{Chapter_HornsRev} was used. The cost weight factors $a$ and $b$ were set to 1 and 0.1 respectively.

It can be seen that the turbines are mostly grouped near the south edge of the boundary and south west corner of the region. This might be attributed to the fact that the water depth is the smallest near those areas. Three turbines are placed in the shallow water just above the southern edge. One turbine was located in the middle of the northern section of the boundary. This was probably done since the gains from increased AEP exceeded the the losses from additional water depth.

\section{Lattice layout}
Similarly to the previous section, the objective function was changed to include the water depth. This can be seen in eq. \ref{eq:Bathymetry_formualtion_lattice}.
%
\begin{equation}
    \label{eq:Bathymetry_formualtion_lattice}
    \begin{aligned}
        & \text{maximize} & & a AEP(\mathbf{w}) - b f_{depth}(\mathbf{w}) \\
        & \text{subject to} & & c_{bound}^{low} \le  c_{bound}(\mathbf{w}) \le c_{bound}^{upp} \\
        &  & & c_{a}^{low} \le  c_{a}(\mathbf{w}) \le  c_{a}^{upp} \\
        &  & & c_{prox}^{low} \le  c_{prox}(\mathbf{w}) \le  c_{prox}^{upp} \\
        & \text{where:} & & \mathbf{w} = \begin{bmatrix}
                                             \alpha & \beta & b_x & b_y & d_x & d_y
                                         \end{bmatrix}
    \end{aligned}
\end{equation}
%

The gradient of the objective function is obtained using chain rule to convert the formulation from cartesian to lattice coordinates, as shown in eq. \ref{eq:BathymetryObjectiveLatticeGradient}.
%
\begin{equation}
  \begin{aligned}
      \nabla f_{obj, lattice} &= a \nabla AEP_{lattice} - b \nabla f_{depth, lattice} \\
			      &= a \nabla AEP_{cartesian} \cdot \frac{\partial x}{w} - b \cdot \nabla f_{depth, cartesian} \frac{\partial x}{w} \\
			      &= \left( a \nabla AEP_{cartesian} - b \nabla f_{depth, cartesian} \right) \cdot \frac{\partial x}{w}
  \end{aligned}
  \label{eq:BathymetryObjectiveLatticeGradient}
\end{equation}
%

%
\begin{figure}[htbp!]
  \centering
    \includegraphics[width=\textwidth]{Figures/Bathymetry_layout_lattice.png}
  \caption{Optimized layout using lattice model}
  \label{fig:Bathymetry_layout_lattice}
\end{figure}
%

Figure \ref{fig:Bathymetry_layout_lattice} shows the wind farm layout obtained by solving the optimization problem described by eq. \ref{eq:Bathymetry_formualtion_lattice}. The initial turbine positions were determined by setting the lattice variables to $\alpha = 0$, $\beta = 90^\circ$, $b_x = 400\text{m}$, $b_y = 400\text{m}$, $d_x = 423000 \text{m}$, $d_y = 6146500 \text{m}$. The lower bound on the proximity constraints was set, like in the previous case, to 560 m. The lattice consists of 16 Vestas V80-2.0MW turbines placed in 4 rows and 4 columns.

Similarly to the previous example, the optimizer placed the turbines in the southern west section of the considered region. This time however, since it was not possible to to arbitrarily position the turbines, some of them ended up at locations with high water depth.

A comparison of the results from both cases is shown in table \ref{tab:Bathymetry_Results}. As expected the simple layout model, which doesn't restrict the positions of the turbines, produced better results. The gross AEP for this wind climate and site conditions was equal to 142,226094 GWh. Moreover the maximum value of the cost function equaled to 120,913987. This was calculated by subtracting the sum of minimum depths of each turbines from the gross AEP value.

%
\begin{table}
%     \begin{center}
      \begin{tabularx}{\textwidth}{|X|X|X|X|X|}\hline
	Layout & Objective function & Obj. fun. relative to maximum [\%] & Net AEP & Wake losses [\%]\\\hline
	Simple & 114,923610 & 4,95 & 139,891515 & 1,44\\\hline
	Lattice & 114,176567 & 5,57 & 139,891515 & 1,64\\\hline
      \end{tabularx}
%     \end{center}
  \caption{Comparison of the obtained results}
  \label{tab:Bathymetry_Results}
\end{table}
%

