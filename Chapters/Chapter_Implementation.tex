% Chapter Template

\chapter{Implementation details} % Main chapter title

\label{Chapter_Implementation} % Change X to a consecutive number; for referencing this chapter elsewhere, use \ref{ChapterX}

\lhead{Chapter III. \emph{Implementation details}} % Change X to a consecutive number; this is for the header on each page - perhaps a shortened title

%----------------------------------------------------------------------------------------
%	SECTION 1
%----------------------------------------------------------------------------------------


\section{Program structure}
%
\begin{figure}[htbp!]
  \centering
    \includegraphics[width=\textwidth]{Figures/Software_stack.png}
  \caption{Software components used by the application}
  \label{fig:Software_stack}
\end{figure}
%	
The application I created during the course of this project was written in Python. It utilizes SciPy \cite{jones2001SciOpeSouSciTooPyt} and NumPy \cite{walt2011Numarrstreffnumcom} for data storage and numerical analysis. Matplotlib \cite{hunter2007Mat2Dgraenv_2} and PyQtGraph for plotting. Sympy \cite{SympyCitation} for symbolic calculations. IPOPT \cite{wachter2006impintfillinalglarnonpro} as an optimization algorithm and PyIpopt \cite{PyipoptCitation} as a Python interface. Watchdog \cite{WatchdogCitation} is used as a utility program to monitor file system events.

Since the application might not run on some systems and different library versions, the precise conditions under which it was tested are shown in the table \ref{tab:Program_versions}.

\begin{table}
    \begin{center}
    \begin{tabular}{|c|c|}\hline
      Item name & Version\\\hline\hline
      Operating system & Ubuntu 14.04.3 LTS \\\hline
      Linux kernel & linux 3.13\\\hline
      Python & 2.7.6 \\\hline
      NumPy & 1.10.1 \\\hline
      SciPy & 0.17.0 \\\hline
      SymPy & 0.7.4.1\\\hline
      Matplotlib & 1.4.3\\\hline
      PyQtGraph & 0.9.10 \\\hline
      Watchdog & 0.8.3\\\hline
    \end{tabular}
    \end{center}
    \caption{Software and package versions}
    \label{tab:Program_versions}
\end{table}

The program was divided into 3 main subgroups: Colonel interface, optimizer interface and the group containing layout models, numerical and symbolic formulations as well as plotting. This was visualized in figure \ref{fig:Software_stack}. Subsequent sections give more detail about the exact implementation. 
The main purpose of this application was to couple the program simulating the wakes effects using Fuga model to an optimization program in order to optimize the layout of an offshore wind farm. Once this was completed it was possible to test different layout models, constraints and types of objective functions and methods of gradient calculation.

%
\begin{figure}[htbp!]
  \centering
    \includegraphics[width=\textwidth]{Figures/Main_algorithm_scheme.png}
  \caption{asldnasl}
  \label{fig:Main_algorithm_scheme}
\end{figure}
%

A graph of the main algorithm is shown in figure \ref{fig:Main_algorithm_scheme}.
At first the program reads the configuration file, sets up a directory structure for Colonel to work on, as well as directories for data and figure output, initializes classes and variables. After that all of the multiprocessing workers are started and their respective queues are set up. This includes workers responsible for running the optimizer, communication with Colonel and plotting. The communication between worker threads is accomplished by means of \texttt{SimpleQueue} from the Python's multiprocessing module and passing specially prepared dictionary, which also contains some metadata information. 
Once the worker running the optimizer interface is initialized, the initial wind farm layout is passed to all of the running Colonel instances. All the information initially required by IPOPT is also computed and passed on at this stage. This include:
%
\begin{itemize}
    \item nvar - number of proble variables
    \item x\_L - lower bounds of the variable vector
    \item x\_U - upper bounds of the variable vector
    \item ncon - number of constraints
    \item g\_L - lower constraint bounds
    \item g\_U - upper constraint bounds
    \item nnzj - number of non-zero terms in the jacobian of constraints matrix
    \item nnzh - number of non-zero terms in the hessian matrix, this was set to 0
    \item eval\_f - call back function handle for calculating the objective value, it takes one single argument x as input vector
    \item eval\_grad\_f - calculates gradient for objective function
    \item eval\_g - calculates the constraint values
    \item eval\_jac\_g - calculates the Jacobi matrix. It takes two arguments, the first is the variable x and the second is a Boolean flag if the flag is true, it supposed to return a tuple (row, col) to indicate the sparse Jacobi matrix's structure. If the flag is false if returns the values of the Jacobi matrix with length nnzj 
    \item eval\_h - calculates the hessian matrix. Function is not used in this project
\end{itemize}
%
An introductory tutorial for running IPOPT can be found at \cite{wachter2009ShoTutGetStaIpo90Min}.

Once the optimization is started IPOPT sends the processed data through PyIpopt interface and the optimization worker to another worker, which I called 'Distributor'. It processes the metadata and determines which Colonel instance receive the data. After Colonel finishes the wake simulation it outputs a file with results to a preset directory, which is being monitored by the Watchdog. This program in turn notifies a worker, which is designed to process the data and pass it on to the distributor. The final step comes to forwarding the data to the optimizer. 

This process continues untill a predetermined conditions are met. Usually this is reaching a value of objective function which does not change with respect to the specified tolerane or exceeding the maximum number of allowed iterations. Of course the optimization can fail as well, for example by converging to a point of local infeasibility. All of the available information about termination statatus can be found at \cite{IPOPTDocumentation}.

Different tasks were realized through the use of multiprocessing module since initially the analytical formulations for AEP gradients were not available and computing those terms using numerical methods can be computationally intensive and time consuming. Using different processes allowed for the computationally demanding tasks to be offset to a high performance server, instead of executing them on a personal computer.


\section{Colonel interface}
% TODO: example of the files used
% TODO: init file
% TODO: input files for AEP, gradients, turbine list
% TODO: output file

% TODO: Worker classes, InputWorker, OutputWorker
% TODO: interface classes: InputFile, OutputFile, basic lexical parser

The communication with Colonel is done using simple ASCII text files. It follows a pattern of using one command with list of arguments per line. Initially a simple setup is required to determine the directories for input and output files, location of the wind climate file, look-up tables, farm data directory as well as other options that do not change throughout the simulation.

A sample file is presented below:
%
\begin{lstlisting}
    set LUT directory "../DTU/Fuga/LUTs-T"
    set input file "ColonelInput.txt"
    set input directory "IN"
    set output directory "OUT"
    set output file "ColonelOutput.txt"
    set farm directory "../DTU/Fuga/Farms"
    set fuga directory "../DTU/Fuga"
    initialize
    load farm "HornsRev1"
    z0 = 0.00010000
    zi = 400.00000000
    zeta0 = 0.00000000
    load wakes
    insert met mast 435253.00000000 6149502.00000000 68.00000000
    Gaussian fit on
    Gradients on
    load wind atlas "HornsRev1/wrf_HRI_55.489N_7.832E_70m.lib"
\end{lstlisting}
%
The interface itself is realized as a Python class representing this Colonel input file. It allows to create methods having higher level of abstraction instead of manually writing specified commands to the input file. An example of this is a function used to determine the value of AEP at a particular set of wind turbine coordinates. Moreover avoiding the realiance on processing single commands could help to implement other wake models without changing the interface.

An example of a command used by the optimizer is \texttt{eval\_f}, which determines the value of the objective function at the position vector passed as an input argument. If the task was to maximize the AEP, then Colonel in this situation should calculate the net AEP of the wind farm at that position. The following input file could be created:
%
\begin{lstlisting}
    move turbine 0 423000.301282938336954 6146575.264503499493003
    move turbine 1 423570.948783198371530 6146574.814210660755634
    move turbine 2 424141.596283458406106 6146574.363917822018266
    move turbine 3 424712.243783718382474 6146573.913624984212220
    move turbine 4 425282.891283978417050 6146573.463332145474851
    calculate AEP
    get total AEP
\end{lstlisting}
%
And as an result the net, gross AEP and the wake losses are outputted in the following form:
%
\begin{lstlisting}
    6.39316201548325E+002  7.11130472214746E+002  0.4564
\end{lstlisting}


\section{Optimizer interface}
% TODO: optimization class
% TODO: interface for different algos needs more or less the same functions (obj, constraints + gradients)

% TODO: pyipopt
% TODO: pyopt


Similarly to the case presented in the previous section, the optimizer interface was implemented using Python's worker processes and classes representing interfaces to specific optimization programs. 
In this project the only optimizer that was used was IPOPT but in the beginning I have also written an interface to pyOpt. However the algorithms it exposes have either poor performance or were unavailable for academic use free of charge.

All of the optimization algorithms used for wind farm layout optimization require at least information about the value of objective function and constraints at particular location. The gradient based methods also need the information about derivatives of those terms. Therefore a simple class can be created that reflects this data and can be used by different optimization programs with little modification to the underlying code structure. The following class serves as a template for the interface between the optimizer and the rest of the program:
\begin{minted}[linenos=true,baselinestretch=1]{python}
    class BaseWFLOP(object):
    
    id = 'optimizer'

    def __init__(self, *args, **kwargs):
        pass

    def initial_setup(self):
        raise NotImplementedError

    def objfun(self, *args, **kwargs):
        raise NotImplementedError

    def confun(self, *args, **kwargs):
        raise NotImplementedError

    def grad_objfun(self, *args, **kwargs):
        raise NotImplementedError

    def jac_confun(self, *args, **kwargs):
        raise NotImplementedError

    def hess_objfun(self, *args, **kwargs):
        raise NotImplementedError

    def hess_confun(self, *args, **kwargs):
        raise NotImplementedError
\end{minted}
All new classes representing WFLOP have to inherit this class in order to be used by the optimizer.


\section{Parallel finite difference scheme}
% TODO: algorithm
Since the interior point method implemented in IPOPT requires gradient of the objective function it would be beneficial to calculate it in parallel, especially as the number of turbines approaches large numbers or extra accuracy is needed when using higher order finite difference algorithm. 

A simplified version of the parallel finite difference method is given below.

\begin{minted}[linenos=true,baselinestretch=1]{python}
    def fd_central_parallel(x, wf, q, num_cpu, fd_order=0, x_pert=1.):
    """ Calculate AEP gradient using finite difference """
    
    # perturbation matrix, assumes x_pert=y_pert
    h = range(-len(CD_COEFF[fd_order])/2, len(CD_COEFF[fd_order])/2 + 1)
    h.remove(0)
    h = np.array(h)*x_pert

    # prepare the data
    x_array = np.zeros((len(h) * len(x), len(x)), dtype=np.float)
    grad_fd = np.zeros((len(h) * len(x),), dtype=np.float)

    for ix in range(len(x)):
        for ic in range(len(CD_COEFF[fd_order])):
            idx = ix*len(h) + ic
            x_tmp = np.array(x)
            x_tmp[ix] += h[ic]
            x_array[idx, :] = x_tmp

    #send the data for processing
    last_idx_x = 0
    items_to_send = [True]*len(x_array)
    items_received = [False]*len(x_array)
    no_items_processing = 0
    cpuid_xrow = [None]*num_cpu

    # send the initial num_cpu items
    for icpu in range(num_cpu):
        q[CPU_ID_MATRIX[icpu]].put(x_array[last_idx_x,:])
        cpuid_xrow[icpu] = last_idx_x
        last_idx_x += 1
        no_items_processing += 1

    # receive the items
    while True:
        # receive the processed data
        data_tmp = q['fd'].get()
        cpu_id = data_tmp.keys()[0]
        cpu_idx = int(cpu_id) - 1

        grad_idx = cpuid_xrow[cpu_idx]
        grad_fd[grad_idx] = data_tmp[cpu_id]
        items_received[grad_idx] = True
        no_items_processing -= 1

        # send data if there are spare cpus
        if no_items_processing < num_cpu \
                and last_idx_x < len(x_array):
            q[ CPU_ID_MATRIX[cpu_idx] ].put(x_array[last_idx_x,:])
            items_to_send[last_idx_x] = False

            cpuid_xrow[cpu_idx] = last_idx_x
            last_idx_x += 1
            no_items_processing += 1

        if np.all(items_received):
            break

    grad_output = np.zeros_like(x)
    for ix in range(len(x)):
        grad_tmp = np.zeros_like(CD_COEFF[fd_order])
        for ic in range(len(CD_COEFF[fd_order])):
            idx = ix*len(h) + ic

            AEP = grad_fd[idx]
            grad_tmp[ic] = AEP * CD_COEFF[fd_order][ic]
        grad_output[ix] = np.sum(grad_tmp) / (1*x_pert)

    return grad_output
\end{minted}


The presented algorithm is based on a similar scheme as a mapreduce function. First the data is prepared in the form of matrices containing perturbed coordinates.  After that the data is sent to each of the available Colonel instances. The variables initialized in rows 21-25 are used for controlling the flow of the algorithm later on. They store the information about the last processed set of coordinates, sent and received data items as well as the table determining which Colonel instance had been allocated to a particular set of coordinates. This information is being used to store the received results at the proper position inside an array. The code in lines 35-57 is responsible for receiving the data and sending additional unprocessed items. This part of the algorithm is stop from executing once all processed items have been received. Lines 59-67 are an implementation of central difference method.