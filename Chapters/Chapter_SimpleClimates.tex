% Chapter Template

\chapter{Numerical experiments with simple wind climates} % Main chapter title
\label{Chapter_SimpleClimates} % Change X to a consecutive number; for referencing this chapter elsewhere, use \ref{ChapterX}

\lhead{Chapter IV. \emph{Numerical experiments with simple wind climates}} % Change X to a consecutive number; this is for the header on each page - perhaps a shortened title

\section{Problem description}
This chapter describes the initial verification of the working models presented earlier. All the turbines were modeled as Vestas V80-2.0 MW units, the same turbines used in Horns Rev 1 wind farm. The power curve of this particular turbine can be seen in \ref{fig:v80_powercurve}. Two simple wind climate groups were used to perform the simulations. 

%
\begin{figure}[htbp!]
  \centering
    \includegraphics[width=\textwidth]{Figures/v80_powercurve.png}
  \caption{Power curve for Vestas V80-2.0MW turbine \cite{v80brochure}}
  \label{fig:v80_powercurve}
\end{figure}
%

The first one involved a wind climate where the wind would be present only in one sector. Its velocity was set to $10\frac{m}{s}$ with a very small deviation from the mean. I prepared several different wind climates based on this description where the only thing that differed was the direction from where the wind was coming.

In the second case wind was present in all the sectors with mean velocity equal to $10\frac{m}{s}$. Some of the wind climates based on this concept had different and nonuniform frequency distribution in the considered sectors. Simulations carried out using those wind climates seemed to produce similar results to the wind climates were the frequency distribution was uniform.

% TODO porownac z test cases z
\citet{mosetti1994Optwinturposlarwinbymeagenalg_2} suggested a similar set of wind climates for which one would perform an optimization of a wind farm. He also included a case with a wind coming from all directions with varying intensity. Those wind climates were later on used by a few other researchers that worked with similar optimization methods, mainly genetic algorithms. In this project however such kind of wind climate would produce very similar results to a wind coming from a single direction, as will be shown in the later part of this report.

% TODO weibull derivation
The wind climates used in this chapter were prepared from a randomly generated set of data points corresponding to a certain wind speed and direction. This data was then used with WAsP Climate Analyst to generate an observed wind climate file. This was a basis for WAsP's Generalized Wind Climate file, from which I could export a Wind Atlas (.lib) file to be used in Fuga calculations.

The same calculations could be done manually to prepare the wind atlas files. This would involve fitting the data to the Weibull statistical distribution function, used by WAsP and Fuga, in order to determine the $A$ and $k$ parameters in the equation:
%
\begin{equation}
    \label{eq:Weibull_distribution_function}
    f(U, A, k) = \frac{k}{A} \left( \frac{U}{A} \right)^{k-1} e^{- \left( \frac{U}{A} \right)^k}
\end{equation}
%
The $A$ and $k$ parameters have to be calculated for every sector and every roughness case. In this project surface roughness equal to $z_0 = 0.0001$ was used, since the simulated wind farms are located at sea.
The exact formulas used to obtain those parameters are given below:
\begin{align*}
    k &= \frac{\sigma_U}{U_m}^{-1.086} \\
    A &= \frac{U_m}{\Gamma \left( 1 + \frac{1}{k} \right)}
\end{align*}
where the standard deviation and the average value of the wind speed can be calculated as:
\begin{align*}
    U_m &= \frac{1}{n} \sum^n_{i=1}U_i \\
    \sigma_U &= \sqrt{\frac{1}{n} \sum^n_{i=1} \left( U_i - U_m \right)^2}
\end{align*}
The exact derivation of the formulations above can be found in \cite{mathew2006Winenefunresanaeco}. Furthermore there is no need to consider the wind shear effects influenced by surface roughness, since the considered wind speed is thought to be measured at the hub height - the same height used by Colonel during wake simulations.



%----------------------------------------------------------------------------------------
%	SECTION 1
%----------------------------------------------------------------------------------------
\section{Single wind direction}
% TODO: describe boundary
% TODO describe constraints

\subsection*{Description of wind climate}
Figures \ref{fig:East_windrose} and \ref{fig:East_velocity} describe the wind climate where wind is predominantly present in the the east sector, i.e. starting from $75^{\circ}$ up to $105^{\circ}$, with 12 sectors each spanning $30^{\circ}$. The wind atlas, represented as an ASCII file used for this wind climate is given below:

%
\begin{lstlisting}
1 1 12
0.0001
70
0.00 0.00 0.00 100.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 0.00 
5.00 5.00 9.93 10.46 10.44 5.00 5.00 5.00 5.00 5.00 5.00 5.00 
2.00 2.00 9.85 10.00 9.95 2.00 2.00 2.00 2.00 2.00 2.00 2.00
\end{lstlisting}
%

The first line describes number of roughness classes, measurement heights and number of sectors in the data set. The 2\textsuperscript{nd} line stores the values of the roughness cases used. It should only contain the exact number of reference roughness lengths as stated in the previous line, otherwise WAsP will not be able to use this file. 3\textsuperscript{rd} line holds the values of the reference heights above ground level. Lines 4-6 has to be repeated for every roughness class and every reference height. Line number 4 stores the frequencies of occurrence for a reference roughness, this data is used to create the wind rose. The next two lines are Weibull $A$ and $k$ parameters corresponding to the wind speed distribution for particular sector.


%
\begin{figure}[htbp]
  \centering
    \includegraphics[width=0.5\textwidth]{Figures/East_windrose.png}
  \caption{Wind rose for a wind climate with wind blowing prevailingly form the east}
  \label{fig:East_windrose}
\end{figure}
%
%
\begin{figure}[htbp]
  \centering
    \includegraphics[width=0.5\textwidth]{Figures/East_velocity.png}
  \caption{Velocity profile for a wind climate with wind blowing prevailingly form the east}
  \label{fig:East_velocity}
\end{figure}
%

%
\begin{figure}[htbp]
  \centering
    \includegraphics[width=0.5\textwidth]{Figures/North_windrose.png}
  \caption{Wind rose for a wind climate with wind blowing prevailingly form the north}
  \label{fig:North_windrose}
\end{figure}
%
%
\begin{figure}[htbp]
  \centering
    \includegraphics[width=0.5\textwidth]{Figures/North_velocity.png}
  \caption{Velocity profile for a wind climate with wind blowing prevailingly form the north}
  \label{fig:North_velocity}
\end{figure}
%

As can be seen in the figures, the wind is present only in a single sector. This simplifies the analysis of the wake effects, since only that direction has to be considered. Furthermore one can guess the possible optimal positions for the wind turbines subjected to this type of wind climate. They should be aligned in a curve in such a way as to avoid getting too close to each other, which could decrease their AEP values.

The wind speed does not play a big part in this simulation, since only one wind direction is considered. It can only influence the length of the resulting wake, i.e the slower the wind speed the shorter the created wake effect will be. The opposite is also true.
The $A$ and $k$ parameters used for the Weibull distribution were chosen in such a way as to imitate a wind speed of $10\frac{\mathtt{m}}{\mathtt{s}}$.


\subsection*{Simple layout model}
Figures \ref{fig:Layout_simple_north} and \ref{fig:Layout_simple_east} show the optimized wind farm layouts. It is evident that the optimizer tries to position the turbines in such a way as to avoid the wake shadowing effects which would decrease AEP. In theory there exist many different optimal solutions for this problem and the presented results are only local solutions. Table \ref{tab:SimpleClimates_Results} demonstrates numerical results for those simulations. 

%
\begin{figure}[htbp!]
  \centering
    \includegraphics[width=0.8\textwidth]{Figures/Layout_simple_north.png}
  \caption{Optimized layout for 12 turbines using simple model with wind coming from the north}
  \label{fig:Layout_simple_north}
\end{figure}
%

%
\begin{figure}[htbp!]
  \centering
    \includegraphics[width=0.8\textwidth]{Figures/Layout_simple_east.png}
  \caption{Optimized layout for 12 turbines using simple model with wind coming from the east}
  \label{fig:Layout_simple_east}
\end{figure}
%

The turbines seem to be aligned in a way which prevents them from being positioned in each other's wakes. 

\subsection*{Lattice layout model}
This section presents the results obtained using the lattice layout model. As in the previous case, the best layout should consist of turbines positioned in a way avoiding the wake effects, which in the simplest form is just a straight line.
%
\begin{figure}[htbp!]
  \centering
    \includegraphics[width=0.8\textwidth]{Figures/Layout_lattice_east.png}
  \caption{Optimized layout for 12 turbines using lattice model with wind coming from the east}
  \label{fig:Layout_lattice_east}
\end{figure}
%

%
\begin{figure}[htbp!]
  \centering
    \includegraphics[width=0.8\textwidth]{Figures/Layout_lattice_north.png}
  \caption{Optimized layout for 12 turbines using lattice model with wind coming from the north}
  \label{fig:Layout_lattice_north}
\end{figure}
%

Table \ref{tab:SimpleClimates_Results} shows the obtained results. In both first cases, were the wind was coming only from the east, it was possible to almost completely avoid wake losses. The simulations carried out with the wind blowing predominantly from the north are characterized by a slight decrease of AEP due to the presence of wake losses. In none of the cases was it possible to eliminate wake losses. This might be contributed to the fact that a tolerance of $\epsilon_{tol} = 10^{-5}$ was used or a numerical error caused by an unusual wind climate, where only one sector is active. 

\begin{table}
    \begin{center}
      \begin{tabular}{|c|c|c|c|c|}\hline
	  Wind Climate & Layout Model & Optimized AEP [GWh] & Gross AEP [GWh] & Wake losses [\%]\\\hline \hline
	  East & Simple & 1,37582766E+02 & 1,37586787E+02 & 0 \\\hline
	  East & Lattice & 1,37585776E+02 & 1,37586787E+02 & 0 \\\hline
	  North & Simple & 1,40470805E+02 & 1,40481346E+02 & 0,01\\\hline
	  North & Lattice & 1,40473140E+02 & 1,40481346E+02 & 0,01 \\\hline
      \end{tabular}
      \caption{Accumulated results for wind climates with winds blowing predominantly from one direction}
    \label{tab:SimpleClimates_Results}
    \end{center}    
\end{table}


%-----------------------------------
%	SUBSECTION 1
%-----------------------------------
\section{Multiple wind directions}
\subsection*{Description of wind climate}
The wind rose and velocity distribution for this wind climate can be seen in figures \ref{fig:Multi_windrose} and \ref{fig:Multi_velocity}. It is almost equally possible to encounter the wind coming from each of the sectors. The velocity distribution is the same for all the sectors. This was done as to avoid the situation where aligning the turbines across one of the directions would produce better results than the others.

%
\begin{figure}[htbp!]
  \centering
    \includegraphics[width=0.5\textwidth]{Figures/Multi_windrose.png}
  \caption{Wind rose for wind climate with wind blowing from all directions}
  \label{fig:Multi_windrose}
\end{figure}
%
%
\begin{figure}[htbp!]
  \centering
    \includegraphics[width=0.5\textwidth]{Figures/Multi_velocity.png}
  \caption{Velocity profile for wind climate with wind blowing from all directions}
  \label{fig:Multi_velocity}
\end{figure}
%

\subsection*{Layout model 1}
Figure \ref{fig:Layout_simple_multi} shows the optimized layout for this wind climate and the simple position model. The turbines seem to be spread out along the outer boundary with a single turbine being located just below the middle of the wind farm. Also a single turbine is present on the bottom part of the boundary, while two turbines were positioned on the opposite side. This arrangement seem to produce better results than when using the lattice layout model, as can be seen in table \ref{tab:MultiClimates_Results}.

%
\begin{figure}[htbp!]
  \centering
    \includegraphics[width=0.8\textwidth]{Figures/Layout_simple_multi.png}
  \caption{Optimized layout for 12 turbines using simple model with wind coming from multiple directions}
  \label{fig:Layout_simple_multi}
\end{figure}
%

\subsection*{Layout model 2}
Figure \ref{fig:Layout_lattice_multi} presents the optimized layout for the wind climate where wind is present in all directions obtained using lattice layout. Unlike in the previous case this model places the turbines in a slightly different locations. It is of course caused by the fact that the nodes in a lattice are positioned in set distances from each other and therefore their resultant coordinates are limited. The wind farm occupies all the available space within the specified boundary. Two turbines were placed in the middle of the boundary. It might be worth to mention that this simulation was performed with specified number of rows and columns equal to 3 and 4. A very similar result is obtained by switching those two values. Naturally this is due to the specific wind climate.

%
\begin{figure}[htbp!]
  \centering
    \includegraphics[width=0.8\textwidth]{Figures/Layout_lattice_multi.png}
  \caption{Optimized layout for 12 turbines using lattice model with wind coming from multiple directions}
  \label{fig:Layout_lattice_multi}
\end{figure}
%

Table \ref{tab:MultiClimates_Results} shows the results for this wind climate. The simple layout seem to produce smaller wake losses overall.
%
\begin{table}
    \begin{center}
      \begin{tabular}{|c|c|c|c|c|}\hline
	  Wind Climate & Layout Model & Optimized AEP [GWh] & Gross AEP [GWh] & Wake losses [\%]\\\hline \hline
	  Multi directional & Simple & 1,77017189E+02 & 1,81183868E+02 & 2,3 \\\hline
	  Multi directional & Lattice & 1,76917141E+02 & 1,81183868E+02 & 2,35 \\\hline
      \end{tabular}
      \caption{Accumulated results for wind climates with winds blowing from all directions}
      \label{tab:MultiClimates_Results}
    \end{center}
\end{table}
%
